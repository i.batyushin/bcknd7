<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.

$user = 'u24234';
$pass = '43523453';
$db = new PDO('mysql:host=localhost;dbname=u24234', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$sel = $db->query("SELECT login FROM lopadmin");
    foreach($sel as $el)
      $login=$el['login'];
  $sel = $db->query("SELECT password FROM lopadmin");
    foreach($sel as $el)
      $pas=$el['password'];

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();

  $errors = array();
  $errors['error'] = !empty($_COOKIE['er_error']);
  if ($errors['error']) {    
    setcookie('er_error', '', 100000);
    $messages[] = '<div class="error">id редактируемого и id удаляемого пользователя не могут совпадать.</div>';
  }

if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']) ||
    $_SERVER['PHP_AUTH_USER'] != $login ||
    md5($_SERVER['PHP_AUTH_PW']) != $pas) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}
print('Вы успешно авторизовались и видите защищенные паролем данные.');

$res=$db->query("SELECT * FROM application_v2");
$sp1=$db->query("SELECT count(*) from Pizza where nomer_pitsi = 1"); 
$sp2=$db->query("SELECT count(*) from Pizza where nomer_pitsi = 2"); 
$sp3=$db->query("SELECT count(*) from Pizza where nomer_pitsi = 3"); 

  ?>
  <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
    </style>
 
<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
<h2>Информация о пользователях</h2>
  <table border="2">
    <tr>
      <td>ID</td>
      <td>ФИО</td>
      <td>E-mail</td>
      <td>Год рождения</td>
      <td>SPower</td>
      <td>After10years</td>
      <td>OcenkaPitsi</td>
      <td>Gender</td>
    </tr>
  <?php foreach($res as $el){ ?>
    <tr>
      <td><?php print($el['id']); ?></td>
      <td><?php print($el['name']); ?></td> 
      <td><?php print($el['email']); ?></td> 
      <td><?php print($el['DateOfBirth']); ?></td> 
      <td><?php print($el['SPower']); ?></td>        
      <td><?php print($el['After10years']); ?></td> 
      <td><?php print($el['OcenkaPitsi']); ?></td> 
      <td><?php print($el['Gender']); ?></td> 
      <?php } ?>
    </tr>
  </table>
  <h2>Статистика по суперспособностям</h2>
  <table border="2">
    <tr>
      <td>Недееспособбность</td> <?php ?>
      <td>Платёжеспособность</td> <?php ?>
      <td>Талантлив(а) во всём</td> <?php ?>
    </tr>
     
    <tr>
      <td><?php foreach($sp1 as $el)
      print($el['count(*)']);
       ?></td>
      <td><?php foreach($sp2 as $el)
      print($el['count(*)']); ?></td>
      <td><?php foreach($sp3 as $el)
      print($el['count(*)']); ?></td>
    </tr>
  </table>

  <br/>

  <form action="" method="post">
    Введите id для редактирования
    <br/>
    <input name="edit" /> 
    <br/><br/>
    Введите id для удаления
    <br/>
    <input name="del" /> 
    <br/><br/>
    <input type="submit" value="Подтвердить" />
  </form>
  <?php


}else
{
  $errors = FALSE;
  if($_POST['edit']==$_POST['del']) { // Если id для удаления совпадает с id для редактирования выдаем ошибочную куку
    setcookie('er_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }

    if ($errors) {
      header('Location: admin.php');
      exit();
    }
    else {
      setcookie('er_error', '', 100000);
    }

    if (!empty($_POST['del'])){ 
      $idd=(int)$_POST['del'];
      $db->query("DELETE FROM Pizza WHERE id = $idd"); 
      $db->query("DELETE FROM loginpass WHERE id = $idd"); 
      $db->query("DELETE FROM application_v2 WHERE id = $idd"); 
      header('Location: admin.php');
    }


    if (!empty($_POST['edit'])){ // Это если поле для редактирования не пусто
    session_start();
    $_SESSION['uid']=(int)$_POST['edit'];
    $_SESSION['login']='Admin'; 
    header('Location: ./');
    }

}

// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********